package com.js.healthcheckapi.model;

import com.js.healthcheckapi.enums.HealthStatus;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class HealthItem {
    private Long id;
    private LocalDate dateCreate;
    private String name;
    private String healthStatus;
    private Boolean isGoHome;

}
