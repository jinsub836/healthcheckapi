package com.js.healthcheckapi.Service;

import com.js.healthcheckapi.entity.Health;
import com.js.healthcheckapi.model.HealthItem;
import com.js.healthcheckapi.model.HealthRequest;
import com.js.healthcheckapi.repository.HealthRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class HealthService {
    private final HealthRepository healthRepository;
    public void setHealth(HealthRequest request){
        Health addData = new Health();
        addData.setDateCreate(LocalDate.now());
        addData.setName(request.getName());
        addData.setHealthStatus(request.getHealthStatus());
        addData.setEtcMemo(request.getEtcMemo());
        healthRepository.save(addData);
    }

    public List<HealthItem> getHealths(){
        List<Health> originlist = healthRepository.findAll();

        List<HealthItem> result = new LinkedList<>();

        for (Health health : originlist){
            HealthItem addItem = new HealthItem();
            addItem.setId(health.getId());
            addItem.setDateCreate(health.getDateCreate());
            addItem.setName(health.getName());
            addItem.setHealthStatus(health.getHealthStatus().getName());
            addItem.setIsGoHome(health.getHealthStatus().getIsGoHome());

            result.add(addItem);
        }

        return result;
    }
}
