package com.js.healthcheckapi.controller;


import com.js.healthcheckapi.Service.HealthService;
import com.js.healthcheckapi.model.HealthItem;
import com.js.healthcheckapi.model.HealthRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/health")
public class HealthController {
    private final HealthService healthService;

    @PostMapping("/new")
    public String setHealth(@RequestBody HealthRequest request) {
        healthService.setHealth(request);
        return "등록 되었습니다.";
    }

    @GetMapping("/all")
    public List<HealthItem> getHealths() {
        return healthService.getHealths();
    }
}
