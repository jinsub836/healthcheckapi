package com.js.healthcheckapi.repository;

import com.js.healthcheckapi.entity.Health;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HealthRepository extends JpaRepository <Health, Long> {
}
